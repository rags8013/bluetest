package com.example.raghu.bt;

import android.os.AsyncTask;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

class PostData extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String[] params) {
        URL url;
        HttpURLConnection connection = null;
        try{
            url = new URL("http://45.55.241.210:8000/register/");

            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");
            connection.setRequestProperty("ACCEPT-LANGUAGE", "en-US,en;0.5");
            connection.setDoOutput(true);
            String urlParams = "studentname="+params[0]+"&fsuid="+params[1]+"&courseid="+params[2]+"&bluetoothid="+params[3];
            OutputStream outputPost = new BufferedOutputStream(connection.getOutputStream());
            outputPost.write(urlParams.getBytes());
            outputPost.flush();
            outputPost.close();
            int responseCode = connection.getResponseCode();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            if(connection!=null){
                connection.disconnect();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
