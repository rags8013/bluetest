package com.example.raghu.bt;

import android.os.Bundle;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Start extends Activity {

    Button submit,turnON,turnOFF;
    BluetoothAdapter bluetooth;
    EditText name,fsuId,courseId,bluetoothId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        submit = (Button) findViewById(R.id.submit);
        turnON = (Button) findViewById(R.id.turnON);
        turnOFF = (Button) findViewById(R.id.turnOFF);
        bluetooth = BluetoothAdapter.getDefaultAdapter();
        name = (EditText) findViewById(R.id.name);
        fsuId = (EditText) findViewById(R.id.fsuID);
        courseId = (EditText) findViewById(R.id.courseID);
        bluetoothId = (EditText) findViewById(R.id.blueAddress);


        submit.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                String userName = name.getText().toString();
                String userID = fsuId.getText().toString();
                String course = courseId.getText().toString();
                String blueId = bluetoothId.getText().toString();
                PostData data = new PostData();
                data.execute(userName,userID,course,blueId);
                name.setText("");
                fsuId.setText("");
                courseId.setText("");
                bluetoothId.setText("");


            }
        });

        turnON.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!bluetooth.isEnabled()) {
                    Toast.makeText(getApplicationContext(),  "Turning ON Bluetooth", Toast.LENGTH_LONG).show();
                    Intent startIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(startIntent,0);
                    Intent discoverIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                    discoverIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION,300);
                    startActivityForResult(discoverIntent,0);
                    String macAddress = android.provider.Settings.Secure.getString(getApplicationContext().getContentResolver(), "bluetooth_address");
                    //Toast.makeText(getApplicationContext(),macAddress,Toast.LENGTH_LONG).show();
                    bluetoothId.setText(macAddress);
                }
            }
        });

        turnOFF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                bluetooth.disable();
                Toast.makeText(getApplicationContext(), "Turning off Bluetooth", Toast.LENGTH_LONG).show();
            }
        });
    }


}