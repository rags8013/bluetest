"""
Webservice for bluetooth attendance
"""
import json
from flask import Flask, render_template, flash, request, jsonify
import Database
from mysql.connector import Error, MySQLConnection

application = Flask(__name__)
application.secret_key = 'many random bytes'


def scandevices(devices, course_id):
    """

    :param devices: List of devices
    :param course_id: Course id for which attendance is being taken
    :return:
    Marks students present based on the presence of their bluetooth device
    """
    db_config = Database.read_db_config()
    try:
        connection = MySQLConnection(**db_config)
        if connection.is_connected():
            print('connected in scanDevices()')
        cursor = connection.cursor()
        for device in devices:
            query = 'select studentId from student_tbl ' \
                    'where bluetoothId = "' + device + '" ' \
                    'and courseId = "' + course_id + '"'
            cursor.execute(query)
            row = cursor.fetchone()
            if row is not None:
                for student_id in row:
                    query = 'select * from attendance_tbl ' \
                            'where studentId = "' + student_id + '" ' \
                            'and DATE(date) = DATE(NOW()) ' \
                            'and courseId ="' + course_id + '"'
                    cursor.execute(query)
                    row = cursor.fetchone()
                    if row is None:
                        query = 'insert into attendance_tbl ' \
                                'values ("' + student_id + '","' \
                                + course_id + '",NOW(),' + '1' + ')'
                        cursor.execute(query)
                        connection.commit()
                    else:
                        query = 'update attendance_tbl set isPresent = 1 ' \
                                'where studentId = "' + student_id + '" ' \
                                'and courseId ="' + course_id + '"'
                        cursor.execute(query)
                        connection.commit()
    except Error as error:
        print(error)
    finally:
        cursor.close()
        connection.close()


@application.route("/getattendancesheet/<course_id>/", methods=["GET", "POST"])
def storedata(course_id):
    """
    Generate report
    :param course_id: Course id
    :return:
    """
    db_config = Database.read_db_config()
    try:
        if request.method == "GET":
            connection = MySQLConnection(**db_config)
            if connection.is_connected():
                print("Connected in storedata")
            cursor = connection.cursor()
            query = 'select studentId,isPresent from attendance_tbl ' \
                    'where DATE(date) = DATE(Now()) ' \
                    'and courseId ="' + course_id + '"'
            cursor.execute(query)
            rows = cursor.fetchall()
            query = 'select count(*) from attendance_tbl where isPresent = 1'
            cursor.execute(query)
            total = cursor.fetchone()
            with open('/home/raghu/pyproject/templates/result.html', 'w') \
                    as myfile:
                myfile.write('<html>')
                myfile.write('<body>')
                myfile.write('<table style="width=100%" border="1">')
                myfile.write('<tr>')
                myfile.write('<th> StudentId </th> &nbsp;')
                myfile.write('<th> IsPresent </th>')
                myfile.write('</tr>')
                for students, ispresent in rows:
                    myfile.write('<tr>')
                    myfile.write('<td>' + students + '</td> &nbsp;')
                    myfile.write('<td>' + str(ispresent) + '</td>')
                    myfile.write('</tr>')
                myfile.write('</table>')
                myfile.write('<h1> Total Present:' + str(total[0]) + '</h1>')
                myfile.write('</body>')
                myfile.write('</html>')
        return render_template("result.html")
    except Error as error:
        print(error)
    finally:
        cursor.close()
        connection.close()


@application.route("/final/", methods=["GET", "POST"])
def finalize():
    """
    Marks students absent, to be called at the end of lecutre
    :return:
    """
    db_config = Database.read_db_config()
    try:
        if request.method == "POST":
            course_id = request.form['courseid']
        connection = MySQLConnection(**db_config)
        if connection.is_connected():
            print("connected in finalizeAttendance")
        cursor = connection.cursor()
        query = 'select distinct studentId from student_tbl ' \
                'where studentId NOT IN ' \
                '(Select studentId from attendance_tbl ' \
                'where courseId = "' + course_id + '" ' \
                'and DATE(date) = DATE(NOW())) ' \
                'and courseId = "' + course_id + '"'
        cursor.execute(query)  # gets all students registered for the course
        rows = cursor.fetchall()
        for row in rows:
            if row is not None:
                for student_id in row:
                    query = 'insert into attendance_tbl ' \
                            'values ("' + student_id + '","' + course_id \
                            + '",NOW(),' + '0' + ')'
                    cursor.execute(query)
                    connection.commit()
        return "<p> Success!! </p>"
    except Error as error:
        print(error)
    finally:
        cursor.close()
        connection.close()
        # notify('CIS5930')


@application.route("/")
def hello():
    """
    Test function, No use
    :return:
    """
    return "<h1 style='color:blue'>Hello There!</h1>"


@application.route("/scan/", methods=["GET", "POST"])
def markattendance():
    """
    Gets list of devices and marks students present.
    :return:
    """
    devices = []
    try:
        if request.method == 'POST':
            results = request.get_json()
            course_id = results[0]['Devices']['courseId']
            for result in results:
                devices.append(result['Devices']['blutoothId'])
            print(devices)
            print(course_id)
            scandevices(devices, course_id)
        return jsonify(results)
    except json.JSONDecodeError as exception:
        flash(exception)
        return "<p>Error</p>"


@application.route('/register/', methods=["GET", "POST"])
def register_page():
    """
    Function used for registering student.
    Takes input from web or mobile application.
    :return:
    """
    error = None
    db_config = Database.read_db_config()
    try:
        if request.method == "POST":
            new_studentname = request.form['studentname']
            new_courseid = request.form['courseid']
            new_fsuid = request.form['fsuid']
            new_bluetoothid = request.form['bluetoothid']

            print(new_studentname)
            print(new_courseid)
            print(new_fsuid)
            print(new_bluetoothid)
            connection = MySQLConnection(**db_config)
            if connection.is_connected():
                print('connected in registration()')
            cursor = connection.cursor()
            query = 'insert into student_tbl ' \
                    'values ("' + new_fsuid + '","' + new_courseid + '","' \
                    + new_studentname + '","' + new_bluetoothid + '")'
            print(query)
            cursor.execute(query)
            connection.commit()
        return render_template("register.html")
    except Error as error:
        flash(error)
        return render_template("register.html", error=error)
        # finally:
        # cursor.close()
        # connection.close()


if __name__ == "__main__":
    application.run(host='0.0.0.0')
