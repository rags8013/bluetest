"""
Notifies students about attendance status
"""
import smtplib


def send_message(number):
    """

    :param number: Phone number
    :return:
    Notifies attendance status by text messages
    Yet to decide usage
    """
    server = smtplib.SMTP("smtp.gmail.com", 587)
    server.ehlo()
    server.starttls()
    server.login('bluetoothattendance.fall16@gmail.com', 'BlueAttend@16')
    server.sendmail('bluetoothattendance.fall16@gmail.com',
                    str(number) + '@tmomail.net',
                    'Test_Message')
    server.close()


def send_mail(result, course_id):
    """

    :param result: Attendance status
    :param course_id: Course number
    :return:
    Notifies attendance status by email
    """
    server = smtplib.SMTP("smtp.gmail.com", 587)
    server.ehlo()
    server.starttls()
    if result[2] == 0:
        message = "You have been marked absent for " \
                  + course_id + " for " + str(result[3])
    elif result[2] == 1:
        message = "You have been marked present for " \
                  + course_id + " for " + str(result[3])
    print(message)
    server.login('bluetoothattendance.fall16@gmail.com',
                 'BlueAttend@16')
    server.sendmail('bluetoothattendance.fall16@gmail.com',
                    result[0] + '@my.fsu.edu',
                    message)
    server.close()
