"""
Establishes database connections
"""
from configparser import ConfigParser


def read_db_config(filename="pyproject.ini", section="mysql"):
    """
    :param filename: File with required data
    :param section: Section of file where data is present
    :return:
    Establishes database connection.
    """
    parser = ConfigParser()
    parser.read(filename)
    database = {}
    if parser.has_section(section):
        items = parser.items(section)
        for item in items:
            database[item[0]] = item[1]
    else:
        raise Exception('{0} not found in {1} file'.format(section, filename))
    return database
