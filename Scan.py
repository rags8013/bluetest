"""
This module scans the for bluetooth devices
and sends them to server, finalizes.
Notify usage yet to be decided.
"""
import sys
import urllib
import bluetooth
from flask import json
from device import Devices
import requests as re
import unittest


def send_data(course_id):
    """

    :param course_id:Course Id of which attendance is taken
    :return:
    Scans for bluetooth devices, send the list to server
    """
    devices = bluetooth.discover_devices(duration=8,
                                         flush_cache=True, lookup_names=True)
    device_list = []
    for device in devices:
        new_device = Devices(device[0], course_id)
        device_list.append(new_device)
    json_str = [d.to_json() for d in device_list]
    url = "http://45.55.241.210:8000/scan/"
    params = json.dumps(json_str).encode('utf8')
    req = urllib.request.Request(url, data=params,
                                 headers={'content-type': 'application/json'})
    response = urllib.request.urlopen(req)
    print(response.status)
    return response.status


def finalize_web(course_id):
    """

    :param course_id: Course Id of which attendance is taken
    :return:
    Finalizes attendance, marks student absent if not present.
    """
    data = {"courseid": course_id}
    response = re.post("http://45.55.241.210:8000/final/", data)
    print(response.status_code)
    return response.status_code


if __name__ == '__main__':
    if sys.argv[1] == '-s':
        send_data(str(sys.argv[2]))
    if sys.argv[1] == '-f':
        finalize_web(str(sys.argv[2]))


class TestCases(unittest.TestCase):
    """
    Test Cases for Scan method
    """

    def test_send_data(self):
        """
        Checks send data method
        :return:
        """
        self.assertEqual(send_data("CIS5930"), 200)

    def test_finalize(self):
        """
        Checks send data method
        :return:
        """
        self.assertEqual(finalize_web("CIS5930"), 200)
