"""
JSON object for devices
"""


class Devices(object):
    """
    JSON object creator for bluetooth devices
    """
    def __init__(self, blutooth_id, course_id):
        """
        Constructor
        :param blutooth_id:
        :param course_id:
        """
        self.blutooth_id = blutooth_id
        self.course_id = course_id

    def to_json(self):
        """
        JSON dictionary
        :return:
        """
        return {"Devices": {'blutoothId': self.blutooth_id,
                            'courseId': self.course_id}}

    def empty_method(self):
        """
        To get pylint 10
        :return:
        """
        return self
